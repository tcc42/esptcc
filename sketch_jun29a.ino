
/**
  Autor: Guilherme Rodrigues
**********************************************************************
  Código Fonte para MCU ESP32
**********************************************************************
  Parte do projeto proposto no TCC de Engenharia Elétrica
**********************************************************************
  SISTEMA DE COMUNICAÇÃO DE DISPOSITIVO USB STORAGE COM DIRETÓRIO REMOTO
**********************************************************************
  Esta parte do projeto é responsável por criar e gerenciar um servidor
  FTP (acesso wireless). Os arquivos deste servidor são armazenados na
  memória deste MCU e enviados em toda inicialização via UART para que
  a outra parte da aplicação tenha acesso.
**********************************************************************
  Universidade de Caxias do Sul (2022)
**/

#include <WiFi.h>
#include "Arduino.h"
#include "FS.h"
#include "FFat.h"
#include <SimpleFTPServer.h>

#define DEFAULT_STORAGE_TYPE_ESP32 STORAGE_FFAT
#define RXD2 16
#define TXD2 17

char u_buff[100];
const char* ssid = "Rodrigues 2.4G";
const char* password = "moztarda10";
int LED_BUILTIN = 2;
FtpServer ftpSrv;   //set #define FTP_DEBUG in ESP8266FtpServer.h to see ftp verbose on serial

void printDirectory(File dir, int numTabs = 3);
void Tx_New_File(const char* file_name);
void Tx_New_Content(String file_content);

void _callback(FtpOperation ftpOperation, unsigned int freeSpace, unsigned int totalSpace)
{
  Serial.print(">>>>>>>>>>>>>>> _callback " );
  Serial.print(ftpOperation);
  /* FTP_CONNECT,
     FTP_DISCONNECT,
     FTP_FREE_SPACE_CHANGE
  */
  Serial.print(" ");
  Serial.print(freeSpace);
  Serial.print(" ");
  Serial.println(totalSpace);

  // freeSpace : totalSpace = x : 360

  if (ftpOperation == FTP_CONNECT) Serial.println(F("CONNECTED"));
  if (ftpOperation == FTP_DISCONNECT) Serial.println(F("DISCONNECTED"));
};
void _transferCallback(FtpTransferOperation ftpOperation, const char* name, unsigned int transferredSize)
{
  Serial.print(">>>>>>>>>>>>>>> _transferCallback " );
  Serial.print(ftpOperation);
  /* FTP_UPLOAD_START = 0,
     FTP_UPLOAD = 1,

     FTP_DOWNLOAD_START = 2,
     FTP_DOWNLOAD = 3,

     FTP_TRANSFER_STOP = 4,
     FTP_DOWNLOAD_STOP = 4,
     FTP_UPLOAD_STOP = 4,

     FTP_TRANSFER_ERROR = 5,
     FTP_DOWNLOAD_ERROR = 5,
     FTP_UPLOAD_ERROR = 5
  */
  Serial.print(" ");
  Serial.print(name);
  Serial.print(" ");
  Serial.println(transferredSize);
};

void setup(void)
{
  pinMode (LED_BUILTIN, OUTPUT);
  Serial.begin(115200);
  Serial2.begin(9600, SERIAL_8N1, RXD2, TXD2);
  delay(2000);
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  /////FTP Setup, ensure FFat is started before ftp;  /////////
  if (FFat.begin(true))
  {
    Serial.println("FFat opened!");

    ftpSrv.setCallback(_callback);
    ftpSrv.setTransferCallback(_transferCallback);

    ftpSrv.begin("esp32", "esp32");   //username, password for ftp.  set ports in ESP8266FtpServer.h  (default 21, 50009 for PASV)
    Serial.println("FTP server started!");
    digitalWrite(LED_BUILTIN, HIGH);
  }
  else
  {
    Serial.println("FFat opened FAIL!!!!!");
  }

  unsigned int totalBytes = FFat.totalBytes();
  unsigned int usedBytes = FFat.usedBytes();
  unsigned int freeBytes  = FFat.freeBytes();

  Serial.println("File sistem info.");

  Serial.print("Total space:      ");
  Serial.print(totalBytes);
  Serial.println("byte");

  Serial.print("Total space used: ");
  Serial.print(usedBytes);
  Serial.println("byte");

  Serial.print("Total space free: ");
  Serial.print(freeBytes);
  Serial.println("byte");

  Serial.println();

  // Open dir folder
  File dir = FFat.open("/");
  // Cycle all the content
  printDirectory(dir);
  Serial2.write(0xC9);
}
void loop(void)
{
  ftpSrv.handleFTP();        //make sure in loop you call handleFTP()!!
  //  Serial2.write(250);
  //  Serial2.print("GUILHERME");
}

void printDirectory(File dir, int numTabs)
{
  while (true)
  {
    File entry =  dir.openNextFile();
    if (! entry)
    {
      // no more files
      break;
    }
    else
    {
      Tx_New_File(entry.name());
      File testFile = FFat.open(entry.name(), "r");
      if (testFile)
      {
        Serial.println("Read file content!");
        /**
           File derivate from Stream so you can use all Stream method
           readBytes, findUntil, parseInt, println etc
        */
        String data1 = testFile.readString();
        Serial.println(data1);
        Tx_New_Content(data1);
        testFile.close();
        delay(200);
      }
      else
      {
        Serial.println("Problem on read file!");
      }
    }
    for (uint8_t i = 0; i < numTabs; i++)
    {
      Serial.print('\t');
    }
    Serial.print(entry.name());
    //    Serial.print("\t Tamanho do nome:");
    //    Serial.print(strlen(entry.name()), DEC);
    if (entry.isDirectory())
    {
      Serial.println("/");
      printDirectory(entry, numTabs + 1);
    }
    else
    {
      // files have sizes, directories do not
      Serial.print("\t\t");
      Serial.println(entry.size(), DEC);
    }
    entry.close();
  }
}

void Tx_New_File(const char* file_name)
{
  unsigned long file_name_size;
  //  char user_buff[100];
  //  user_buff[0] = 0x01; // 1 em ascii
  //  user_buff[1] = 0x01; // linha 1
  //  user_buff[2] = '\0';
  //
  //  strcat(user_buff, file_name);
  //  for (int i = 0; user_buff[i] != '\0'; i++)
  //  {
  //    Serial.print("\nData[");
  //    Serial.print(i, DEC);
  //    Serial.print("] = ");
  //    Serial.print(user_buff[i]);
  //  }
  //  Serial.print("\n");
  //  Serial2.print(user_buff);
  file_name_size = strlen(file_name);
  Serial2.write(0xCA);
  Serial2.write(file_name_size);
  Serial2.print(file_name);
  Serial2.write('\0');
  Serial2.write(0xC6);
}

void Tx_New_Content(String file_content)
{
  unsigned long content_size, content_size_lsb, content_size_msb;
  content_size = file_content.length();
  content_size_lsb = content_size & 0xFF;
  content_size_msb = (content_size >> 8) & 0xFF;
  Serial2.write(0xDA);
  Serial2.write(content_size_msb);
  Serial2.write(content_size_lsb);
  Serial2.print(file_content);
  Serial2.write('\0');
  Serial2.write(0xD6);
}
